(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.progress_bar = {
        attach: function (context, settings) {
            var percentage = drupalSettings.field_completeness.percentage;
            var included_fields = drupalSettings.field_completeness.included_fields;
            $('.percentage-text', context).text(percentage);
            $("#progressbar", context).progressbar({value: percentage});
            $('#progressbar div.ui-progressbar-value', context).css('width', percentage + "%");
        }
    }
})(jQuery, Drupal, drupalSettings);