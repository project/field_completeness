CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
   * Paragraphs
   * Override progressbar percentage
   
 
INTRODUCTION
------------
This module provides a progression bar with percentage to specify content editors to provide sufficient information to present their content.
Supports paragraphs entity reference fields to include in field completeness. If the reference entities have more than one entity, it will consider the first entity to provide the progression bar.

REQUIREMENTS
------------

No special requirements.

RECOMMENDED MODULES
-------------------

 * A wrapper module around the jQuery UI(https://www.drupal.org/project/jquery_ui) effects library that lets module developers add swooshy, swishy effects to their code.
 * jQuery UI Progressbar(https://www.drupal.org/project/jquery_ui_progressbar) library for any themes and modules that require it.

INSTALLATION
------------

The installation of this module is like other Drupal modules.

 1. Install the module using Composer: composer require drupal/field_completeness
 2. Enable the module using Administration Interface(Administration -> Extend) or drush en field_completeness
 3. It will install jquery_ui and jquery_ui_progressbar contrib modules if you don't have it

CONFIGURATION
-------------
 1. Once you enable the module, it will create a custom block to display field completeness progression bar
 2. Goto -> Administration -> Structure -> Block layout(admin/structure/block)
 3. Click on 'Place Block' button associated with each available region and Specify pages by using their paths (/node/*(Node view page) and /node/*/edit(Node edit page))
 4. Goto -> Administration -> Structure -> Field completeness (admin/structure/fc/settings)
 5. Choose the content type that the field completeness strength bar needs.
 6. Goto -> Administration -> Structure -> Content types (/admin/structure/types)
 6. Goto -> Manage Fields and Edit the field to include in field Field completeness
  View field completness progression bar on Node View/ Edit page
 
 PARAGRAPHS
 ----------
 If you have chosen paragraph entity field for progress bar, follow below steps
  1. Include the paragraph entity reference fields as per the preceding steps.
  2. Goto -> Administration -> Structure -> Paragraph Types
  3. Goto -> Manage Fields and Edit the field to include in field Field completeness
  View field completness progression bar on Node View/ Edit page
 
 OVERRIDE PROGRESSBAR PERCENTAGE
 -------------------------------
  Below code will help to override percentage for a field:
  
  $entity = $form_state->getFormObject()->getEntity();
  $nid = $entity->id();
  $field_value = $entity->field_name->value;
  if (!empty($nid)) {
    if (\Drupal::moduleHandler()->moduleExists('field_completeness')) {
      /** @var \Drupal\field_completeness\FieldCompletenessManager $field_completeness_manager */
      $field_completeness_manager = \Drupal::service('field_completeness.manager');
      if (in_array('field_name', $field_completeness_manager->getIncludedFields($entity->bundle()))) {
        $fc_data = $field_completeness_manager->fieldCompletenessStorage->select($nid);
        $fc_values['completeness'] = $field_completeness_manager->getPhpUnSerialized($fc_data[$nid]['completeness']);
        $fc_values['completeness']['field_name'] = !empty($field_value) ?? 0; //Modify as per your value
        $field_completeness_manager->fieldCompletenessStorage->update($nid,  $field_completeness_manager->calculatePercentageForExistingItem($fc_values));
      }
    }
  }