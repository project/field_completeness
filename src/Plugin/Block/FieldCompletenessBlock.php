<?php

namespace Drupal\field_completeness\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a 'FieldCompletenessBlock' block.
 *
 * @Block(
 *  id = "field_completeness_block",
 *  admin_label = @Translation("Field Completeness"),
 * )
 */
class FieldCompletenessBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build = [];
    $percentage = 0;
    $included_fields = [];
    /** @var \Drupal\field_completeness\FieldCompletenessManager $field_completeness_manager */
    $field_completeness_manager = \Drupal::service('field_completeness.manager');
    $node = $field_completeness_manager->getNode();
    if ($node) {
      $percentage = $this->getPercentage();
      $included_fields = $field_completeness_manager->getIncludedFields($node->bundle());
    }
    else {
      $included_fields = $field_completeness_manager->getIncludedFields($field_completeness_manager->getEntityBundleFromRoute());
    }


    $block = [
      '#theme' => 'field_completeness_block'
    ];
    $block['#attached']['drupalSettings']['field_completeness']['percentage'] = $percentage;
    $block['#attached']['drupalSettings']['field_completeness']['included_fields'] = $included_fields;
    $block['#attached']['library'][] = 'field_completeness/progressBar';
    $build['field_completeness_block'] = $block;
    return $build;
  }

  /**
   * gets percetage of completed field from DB table
   */
  public function getPercentage() {
    $percentage = 0;
    /** @var \Drupal\field_completeness\FieldCompletenessManager $field_completeness_manager */
    $field_completeness_manager = \Drupal::service('field_completeness.manager');
    $node = $field_completeness_manager->getNode();
    if ($node) {
      $percentage = $field_completeness_manager->getStaticPercentage($node);
    }
    return (int) $percentage;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    /** @var \Drupal\field_completeness\FieldCompletenessManager $field_completeness_manager */
    $field_completeness_manager = \Drupal::service('field_completeness.manager');
    $node = $field_completeness_manager->getNode();
    $route_name = \Drupal::routeMatch()->getRouteName();

    $bundle = "";

    if ($node) {
      $bundle = $node->bundle();
    }
    else {
      $bundle = $field_completeness_manager->getEntityBundleFromRoute();
    }
    if ($route_name == 'node.add') {
      // Forbid access.
      return AccessResult::forbidden();
    }

    if (!empty($bundle) && $field_completeness_manager->isAllowedContentType($bundle)) {
      return AccessResult::allowed();
    }
    else {
      // Forbid access.
      return AccessResult::forbidden();
    }
  }

  /**
   * {@inheritdoc}
   * @return int
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
