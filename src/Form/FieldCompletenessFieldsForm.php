<?php

namespace Drupal\field_completeness\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Configure Field completeness fields settings for this site.
 *
 * @internal
 */
class FieldCompletenessFieldsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fc_admin_fields_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $node_type = NULL) {
    $form['actions'] = [
      '#type' => 'actions',
    ];
    if (!empty($node_type)) {
      $config = $this->config('field_completeness.node.' . $node_type . '.settings');
      $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $node_type);
      $options = [];
      $selected_value = [];
      $para_options = [];

      foreach ($field_definitions as $field_config) {
        if ($field_config instanceof FieldConfig) {
          $type_label = $field_config->getType();
          //Entity type revisions
          if ($field_config->getType() == 'entity_reference_revisions') {
            //Get field storage config
            $field_storage = FieldStorageConfig::loadByName($field_config->getTargetEntityTypeId(), $field_config->getName());
            if (!is_null($field_storage) && $field_storage->getSetting('target_type') == 'paragraph') {
              $paragraph_types = $field_config->getSetting('handler_settings')['target_bundles'];
              if (!empty($paragraph_types)) {
                foreach ($paragraph_types as $paragraph_type) {
                  $para_field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($field_storage->getSetting('target_type'), $paragraph_type);
                  $para_options = [];
                  foreach ($para_field_definitions as $para_field_config) {
                    if ($para_field_config instanceof FieldConfig) {
                      $para_options[$para_field_config->getName()] = $para_field_config->getLabel();
                    }
                  }
                  $form['fc_entity_' . $field_config->getName()]['fc_allowed_para_fields'] = [
                    '#type' => 'checkboxes',
                    '#title' => t('Options for @type', array('@type' => $field_config->getLabel())),
                    '#options' => $para_options,
                    '#multiple' => TRUE,
                    '#parents' => ['fc_allowed_fields'],
                    '#states' => [
                      'visible' => [
                        ':input[name="fc_allowed_fields[' . $field_config->getName() . ']"]' => ['checked' => TRUE],
                      ],
                    ],
                  ];
                }
              }
            }
          }
          $options[$field_config->getName()] = $field_config->getLabel();
        }
      }

      /** @var \Drupal\field_completeness\FieldCompletenessManager $field_completeness_manager */
      $field_completeness_manager = \Drupal::service('field_completeness.manager');

      foreach ($field_completeness_manager->getIncludedFields($node_type) as $field_name) {
        $selected_value[$field_name] = $field_name;
      }

      $form['fc_allowed_fields'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Select the fields to inlucde in field completeness for %type', ['%type' => $node_type]),
        '#default_value' => $selected_value,
        '#options' => $options,
        '#required' => TRUE,
        '#tree' => TRUE,
      ];

      $form['fc_allowed_type'] = [
        '#type' => 'hidden',
        '#value' => $node_type,
      ];
    }
    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#attributes' => [
        'class' => ['button--primary'],
      ],
      '#value' => $this->t('Save configuration'),
      '#description' => $this->t('Save configuration, #type = submit'),
    ];

    // Add a reset button that handles the submission of the form.
    $form['actions']['reset'] = [
      '#type' => 'button',
      '#button_type' => 'reset',
      '#value' => $this->t('Reset'),
      '#description' => $this->t('Reset'),
      '#attributes' => [
        'onclick' => 'this.form.reset(); return false;',
        'class' => ['button--primary'],
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->isValueEmpty(['fc_allowed_fields'])) {
      $form_state->setErrorByName('fc_allowed_fields', $this->t('The content type for the field completeness must be one of those selected as an allowed field completeness type.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\field_completeness\FieldCompletenessManager $field_completeness_manager */
    $field_completeness_manager = \Drupal::service('field_completeness.manager');
    $allowed_types = [];

    $allowed_fields = array_filter($form_state->getValue('fc_allowed_fields'));
    $allowed_fields = array_map(function($field) {
      return (int) 1;
    }, $allowed_fields);
    $bundle = $form_state->getValue('fc_allowed_type');
    $allowed_types = (array) $field_completeness_manager->getAllowedContentTypes();
    if (!in_array($bundle, $allowed_types)) {
      $type_config = \Drupal::service('config.factory')->getEditable('field_completeness.settings');
      $allowed_types[] = $bundle;
      $type_config->set('allowed_types', $allowed_types)->save();
    }

    $config = \Drupal::service('config.factory')->getEditable('field_completeness.node.' . $bundle . '.settings');
    $config->set('node', $allowed_fields)->save();

    $this->messenger()->addStatus($this->t('Saved configuration'));
  }

}
