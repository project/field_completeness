<?php

namespace Drupal\field_completeness;

use Drupal\Core\Database\Connection;

/**
 * Defines a storage class for Field completeness.
 */
class FieldCompletenessStorage {

  /**
   * Database service object
   *
   * @var Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a field completeness storage Object
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * Determines entity exists or not in field completeness table
   */
  public function getCount($nid, $lang_code) {
    $select = $this->connection->select('field_completeness', 'fc');
    $select->condition('entity_id', $nid, '=');
    if (!empty($lang_code)) {
      $select->condition('langcode', $lang_code, '=');
    }
    return $select->countQuery()->execute()->fetchField();
  }

  /**
   * Insert field completeness data
   */
  public function insert($data) {

    return $this->connection
        ->insert('field_completeness')
        ->fields($data)
        ->execute();
  }

  /**
   * Update records for a node
   */
  public function update($nid, $fields) {
    return $this->connection
        ->update('field_completeness')
        ->fields($fields)
        ->condition('entity_id', $nid)
        ->execute();
  }

  /**
   * Loads a record by node id
   */
  public function select($nid) {
    return $this->connection
        ->query("SELECT * FROM {field_completeness} WHERE entity_id = :nid", [':nid' => $nid])
        ->fetchAllAssoc('entity_id', \PDO::FETCH_ASSOC);
  }

  /**
   * Loads a record by node bundle
   */
  public function selectByBundle($bundle) {
    return $this->connection
        ->select('field_completeness', 'fc')
        ->condition('fc.entity_type', $bundle, '=')
        ->fields('fc', ['entity_id', 'completeness', 'percentage', 'complete'])
        ->execute()->fetchAllAssoc('entity_id', \PDO::FETCH_ASSOC);
  }

  /**
   * Sync nodes
   */
  public function delete($nid) {
    return $this->connection
        ->delete('field_completeness')
        ->condition('entity_id', $nid, '=')
        ->execute();
  }

}
